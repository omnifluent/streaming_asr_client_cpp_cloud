## AppTek Streaming ASR API C++

This sample application demonstrates how to use the AppTek Streaming ASR in the cloud.  This sample code is written in C++ but other sample clients are available for Python, Java, Node and C#.

The streaming API utilizes both HTTPS and gRPC for authenticating and communicating with the API services.

**Building the sample code**
The project includes both Visual Studio 2019 project and gcc Makefile.
Library dependencies for this sample code:

 - gRPC and all its dependencies; protobuf, openssl, absl, etc.
 - curl
 
These packages can be built and installed separately but this project utilizes [vcpkg](https://vcpkg.io/en/index.html) for easy dependencies building.
After installing vcpkg, the following command will build and install necessary dependencies.

    ./vcpkg install grpc curl
Building on Windows
Load the streaming_asr_client_c++_cloud.sln solution file and build the Debug configuration.

Building on Linux
Expects gcc version which supports C++14 standard and the Makefile uses some environment variables to locate the vcpkg libraries.  Modify the **VCPKG_DIR** variable to point to the correct location of the vcpkg packages.  Run make.\

**Running**
The sample application uses the following command line arguments to set options:

|Option|Description
|--|--|
| -f | Set the raw audio data file name for processing.  If this option is not set, the application will perform a *GetAvailable* request.
| -k | Set the AppTek API key
| -l | Set the audio language.  E.g. en-US
| -r | Set the audio sample rage.  E.g. 16000
| -t | Set the language to translate recognition results to.  E.g. es

## The API services
The API is a two-step process:

 1. Authenticate
 2. Processing

**Authentication**
To gain access to the streaming service gateway, the client must first validate their API key by performing an HTTPS request to the AppTek licensing service. 
The URL for the license services is https://license.apptek.com/license/v2/token/{api_key} where **api_key** is the API key provided by AppTek.
If the API key is successfully validated, a JWT access token is returned which contains information about the streaming service gateway.  The access token contains three sections separated by a period.  Each section is Base64url encoded.

    {token prefix}.{gateway information}.{token suffix}
The client must extract the gateway information by decoding the second token section, which contains a json object.  The most important values in the json object are **host** and **port** which contain the TCP endpoint for the sreaming service gateway.

**Processing**
The streaming service gateway provides two methods:
	GetAvailable function which queries the gateway for available resources.
	RecognizeStream function which receives audio data and returns real-time transcription results.

**GetAvailable**
Create a *AvailableRequest* object and the *license_token* value to the full token received from the authentication API and submit the request.  The service will return a structure containing; language; audio sample rate; text domain and available languages to translate into. E.g.

|language|audio sample rate|text domain|translate to languages
|--|--|--|--
| en-US | 16000 | default | af,ar,be,bg,bs,cs,da,de,el,es,es_latam,et,fa,fi,fr,he,hi,hr,hu,id,is,it,ja,ko,lt,lv,mk,ms,nl,no,pl,ps,pt,pt_br,ro,ru,sk,sl,sr,sv,th,tl,tr,uk,ur,zh,zh_tw
| en-US | 8000 | default | af,ar,be,bg,bs,cs,da,de,el,es,es_latam,et,fa,fi,fr,he,hi,hr,hu,id,is,it,ja,ko,lt,lv,mk,ms,nl,no,pl,ps,pt,pt_br,ro,ru,sk,sl,sr,sv,th,tl,tr,uk,ur,zh,zh_tw

**RecognizeStream**
To start a new recognition, first create and send a *RecognizeStreamRequest* object and set the *RecognizeStreamConfig* object properties.  This configuration structure must be the first message sent to the gateway.  View the **apptek.proto** file for details on the various message structures.

    message RecognizeAudioConfig {
	    RecognizeAudioEncoding encoding = 1; // encoding of the audio data
	    uint32 sample_rate_hz = 2; // sample rate of the audio data in Hz
	    string lang_code = 3; // language code of the audio
	    string domain = 4; // domain name (leave empty if not sure)
	    google.protobuf.UInt32Value mutable_suffix_length = 5; // maximum mutable suffix length (leave empty if not sure)
    }
Currently only 16bit raw PCM audio (*PCM_16bit_SI_MONO*) is accepted by the gateway.  The audio sample rate must match that set in the *RecognizeStreamConfig*.
Optionally set the *RecognizeTranslateConfig* with the language to translate.  If the *translate_interval* set in the *translate_configuration* is set to *TRANSLATION_INTERVAL_CONTINUOUS*, translation of every recognition result is performed.  Otherwise, one the *final* recognition result is translated.

Once the configuration message has been sent, messages containing raw audio data are sent with the same *RecognizeStreamRequest* object with the audio data in the *audio* value.
The response from the services is contained in the *RecognizeStreamResponse* object.

    message RecognizeStreamResponse {
	    oneof oneof_response {
		    RecognizeTranscription transcription = 1; // a transcription response
		    RecognizeSegmentEnd segment_end = 2; // a segment end response
		    RecognizeProgressStatus progress_status = 3; // a progress status response
		    RecognizePostprocessing postprocessing = 4; // a postprocessing response
		    RecognizeTranslation translation = 5; // a translation response
	    }
    }
As the message structure above shows, the response from the service can contain one or more of the following data structures:

 - transcription - The raw ASR output results which includes individual word timing information.  The results are considered *partial results* because the output is subject to change until the end of the segment.
 - segment_end - Signals the end of segment.
 - progress_status	- Includes information about the ASR engine processing.
 - postprocess - Provides post processed recognition text which includes punctuation and capitalization.
 - translation	- Translated text if translation language is set.

While postprocess and translation results can be sent for *partial results*, these messages are only guaranteed for the *final* segment.

**Troubleshooting**
gRPC provides a tracing mechanism which will provide information about the communication between the client and the gateway.  Set the environment variable "GRPC_VERBOSITY=DEBUG" to enable detailed output.
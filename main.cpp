// streaming_asr_client_c++_cloud.cpp
// Demonstrates how to use the AppTek Streaming API from C++
// Requires:
//  gRPC
//  protobuf
//  curl
//  jsoncpp
//  cpp-base64
//
#include <string>
#include <iostream>
#include <fstream>
#include <thread>

#include "APILicense.h"
#include "APIClient.h"

using namespace std;
using namespace AppTek;

string _APIKey = "";
string _AudioFilename = "";
int _AudioSampleRate = 16000;
string _ASRLanguage = "";
string _TranslateTo = "";

#define option_value(val,index) do { \
    if (++index < argc) { \
        val = argv[index]; \
    } else { \
        std::cerr << "Missing value for \"" << argv[i] << "\" option\n"; \
        return -1; \
    } }while(0)

int get_command_line(int argc, char* argv[])
{
    std::string tmp;
    for (int i = 0; i < argc; i++)
    {
        if (strcmp(argv[i], "-f") == 0)
            option_value(_AudioFilename, i);
        else if (strcmp(argv[i], "-k") == 0)
            option_value(_APIKey, i);
        else if (strcmp(argv[i], "-l") == 0)
            option_value(_ASRLanguage, i);
        else if (strcmp(argv[i], "-r") == 0)
        {
            option_value(tmp, i);
            int rate = atoi(tmp.c_str());
            if (rate != 16000 && rate != 8000)
            {
                std::cerr << "Invalid audio sample rate " << tmp << "\n";
                return -1;
            }
        }
        else if (strcmp(argv[i], "-t") == 0)
            option_value(_TranslateTo, i);
    }

    return 0;
}

// Output transcription results to console
void OutputRecognitionResults(gateway::RecognizeStreamResponse & response)
{
    if (response.has_postprocessing())
        cout << "Final:   " << response.postprocessing().postprocessed() << "\n";
    else if (response.has_translation())
        cout << "Translate: " << response.translation().translation() << "\n";
    else if (response.has_transcription())
    {
        cout << "Partial: ";
        const gateway::RecognizeTranscription & transcription = response.transcription();
        for (int i = 0; i < transcription.words_size(); i++)
            cout << transcription.words(i).word() << " ";
        cout << "\n";
    }
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        cerr << "Insufficient command line options\n";
        return -1;
    }

    if (get_command_line(argc - 1, &argv[1]))
        return -1;

    // get license token
    APILicense apiLicense(_APIKey);

    if (!apiLicense.Token().IsValid())
    {
        cerr << "License failed - " << apiLicense.ErrorMessage();
    }
    else
    {
        // Create our client object
        APIClient apiClient(apiLicense.Token());

        // if audio filename is present we will perform a recognition
        if (_AudioFilename.length())
        {
            if (_ASRLanguage.length() == 0)
            {
                cerr << "ASR language required.\n";
                return 0;
            }

            // send 250 msec buffers
            std::vector<char>audio_data(_AudioSampleRate * 2 / 4);
            // audio file is required to be RAW 16bit PCM in matching sample rate
            std::ifstream audioFile;
            audioFile.open(_AudioFilename, std::ios_base::in | std::ios_base::binary);
            if (audioFile)
            {
                std::unique_ptr<RecognizeStream> recognizeStream =
                    apiClient.CreateRecognizeStream(_ASRLanguage, _AudioSampleRate, _TranslateTo);
                if (!recognizeStream)
                {
                    cerr << "Error creating recognizer stream\n";
                    return 0;
                }
                bool ok = true, err = false;
                int delay = 0;
                gateway::RecognizeStreamResponse response;
                do
                {
                    // simulate realtime by pausing for the 
                    // duration of the last audio packet sent
                    if (delay)
                        std::this_thread::sleep_for(std::chrono::milliseconds(delay));

                    // check if we can send an audio packet
                    if (recognizeStream->CanWrite(err))
                    {
                        size_t read = 0;
                        audioFile.read(audio_data.data(), audio_data.size());
                        read = audioFile.gcount();
                        if (read)
                        {
                            ok = recognizeStream->Stream(audio_data, read);
                            if (ok)
                                delay = read / ((_AudioSampleRate / 1000) * 2);
                        }
                        else
                            audioFile.close();
                    }

                    // check if a response was received
                    if (!err && recognizeStream->Response(response, err))
                        OutputRecognitionResults(response);

                } while (audioFile && !err);
                
                audioFile.close();

                // We won't be writing any more audio so try to
                // close our write channel.
                // If this returns false, it means there are still
                // writes pending
                while (!err && !recognizeStream->WritesDone(err))
                {
                    if (recognizeStream->Response(response, err) && !err)
                        OutputRecognitionResults(response);
                    else
                        std::this_thread::sleep_for(std::chrono::milliseconds(100));
                }

                // read any remaining responses
                // err is set when channel is shutdown
                while (!err)
                {
                    if (recognizeStream->Response(response, err))
                        OutputRecognitionResults(response);
                    else
                        std::this_thread::sleep_for(std::chrono::milliseconds(100));
                }
            }
            else
                cerr << "Error opening audio file\n";
        }
        else
        {
            // get list of available resources
            gateway::RecognizeAvailableResponse response;
            if (apiClient.GetAvailable(response))
            {
                for (int i = 0; i < response.list_size(); i++)
                {
                    gateway::RecognizeAvailable const& available = response.list(i);
                    cout << available.language().lang_code() << " ("
                        << (int)available.language().direction() << "), "
                        << available.domain() << ", "
                        << available.sample_rate_hz();
                    if (available.mt_target_languages_size())
                    {
                        cout << ", [";
                        for (int j = 0; j < available.mt_target_languages_size(); j++)
                        {
                            gateway::Language const& translate_to =
                                available.mt_target_languages(j);
                            if (j)
                                cout << ",";
                            cout << translate_to.lang_code();
                        }
                        cout << "]\n";
                    }
                    else
                        cout << "\n";
                }
            }
            else
                cerr << "Failed to retrieve gateway available resources\n";
        }
    }   

    return 0;
}
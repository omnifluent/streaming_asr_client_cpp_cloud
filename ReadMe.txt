Windows
Use vcpkg to install grpc dependencies - https://vcpkg.io/en/getting-started.html
.\vcpkg install grpc curl

Create gRPC classes
Add directory for protoc tool to PATH
 set PATH=%PATH%;%VCPKG_DIR%\packages\protobuf_x64-windows\tools\protobuf
Run
 protoc --cpp_out=win/proto apptek.proto
 protoc --grpc_out=win/proto --plugin=protoc-gen-grpc=%VCPKG_DIR%\packages\grpc_x64-windows\tools\grpc\grpc_cpp_plugin.exe apptek.proto

** NOTE **
If the version of the gRPC package is changed, the gRPC classes MUST be recreated
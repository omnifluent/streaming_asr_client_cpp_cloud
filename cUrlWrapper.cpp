// Simple cURL wrapper to manage cURL read/write methods
#include "cURLWrapper.h"
#include <atomic>
#include <sstream>

cURLWrapper::cURLWrapper()
{
	Ref();
	_curl = curl_easy_init();
	if (_curl)
	{
		curl_easy_setopt(_curl, CURLOPT_WRITEFUNCTION, cURLWrite);
		curl_easy_setopt(_curl, CURLOPT_WRITEDATA, this);
	}
}

cURLWrapper::~cURLWrapper()
{
	if (_curl)
		curl_easy_cleanup(_curl);
}

void cURLWrapper::Ref()
{
	static std::atomic<bool>init(false);

	if (!init.exchange(true))
		curl_global_init(CURL_GLOBAL_ALL);
}

cURLWrapper::operator CURL* () const
{
	return _curl;
}

size_t cURLWrapper::cURLWrite(void* ptr, size_t size, size_t nmemb, void* user_data)
{
	cURLWrapper* curl  = static_cast<cURLWrapper*>(user_data);

	if (curl->_writeSink == NULL)
		curl->_writeSink = std::make_shared<std::stringstream>();
	
	if (curl->_writeSink && *curl->_writeSink)
	{
		curl->_writeSink->write(static_cast<const char*>(ptr), size * nmemb);
		return (curl->_writeSink->bad()) ? 0 : size * nmemb;
	}

	return 0;
}

CURLcode cURLWrapper::Perform()
{
	CURLcode curl_code = CURLE_FAILED_INIT;
	if (_curl)
	{
		curl_code = curl_easy_perform(_curl);
		if (curl_code != CURLE_OK)
			_errorMessage = curl_easy_strerror(curl_code);
	}
	return curl_code;
}

std::shared_ptr<std::iostream> cURLWrapper::ResponseStream()
{
	return _writeSink;
}

#pragma once
#include "APIToken.h"
#include "contrib/jsoncpp/json/json.h"
#include <grpc++/channel.h>
#include "proto/apptek.pb.h"
#include "proto/apptek.grpc.pb.h"

namespace AppTek {

	class RecognizeStream;

	class APIClient
	{
		const APIToken& _api_token;
		std::shared_ptr<grpc::Channel> _grpc_channel;

		APIClient() = delete;
	public:
		APIClient(const APIToken& api_token);
		~APIClient();
		bool IsConnected();
		bool GetAvailable(gateway::RecognizeAvailableResponse& response);
		std::unique_ptr<RecognizeStream> CreateRecognizeStream(
			std::string const& language, int sample_rate,
			std::string const& translate_to_language);
	};

	class RecognizeStream
	{
		friend class APIClient;
	protected:
		std::unique_ptr<grpc::ClientContext> _context;
		std::unique_ptr<gateway::Gateway::Stub> _gateway;
		//std::unique_ptr<::grpc::ClientReaderWriterInterface< ::gateway::RecognizeStreamRequest, ::gateway::RecognizeStreamResponse> >
		//	_stream;
		std::unique_ptr<::grpc::ClientAsyncReaderWriterInterface< ::gateway::RecognizeStreamRequest, ::gateway::RecognizeStreamResponse> >
			_stream;
		grpc::CompletionQueue _grpc_cq;
		gateway::RecognizeStreamResponse _response, _next_response;
		bool _write_pending = false, _read_pending = false, _writes_done = false;

		RecognizeStream() = delete;

	public:
		RecognizeStream(std::shared_ptr<grpc::Channel>& grpc_channel);
		~RecognizeStream();

		// Call to check if we can send the next audio packet
		bool CanWrite(bool& error);
		// Send audio data and get a response message if available
		// If audio_data is not empty, sends the audio data to the gateway
		// return True if response received
		bool Stream(std::vector<char>audio_data, size_t length);// , gateway::RecognizeStreamResponse& response);

		bool Response(gateway::RecognizeStreamResponse& response, bool& error);

		// err = True if failure
		// returns false if there are pending writes
		bool WritesDone(bool& error);
	};

} // namespace AppTek
// Parse and decode client-side portion of AppTek Streaming ASR API token
#pragma once
#include "contrib/jsoncpp/json/json.h"

namespace AppTek {

	class APIToken
	{
		std::string _str_token;
		Json::Value _api_token_json;
		std::string _error;

	public:
		APIToken();
		APIToken(std::string const& str_token);
		bool Set(std::string const& str_token);

		std::string ErrorMessage() const
		{
			return _error;
		}

		bool IsValid() const;
		std::string Token() const;
		std::string Gateway() const;
	};

} // namespace AppTek

// Simple cURL wrapper to manage cURL read/write methods
#pragma once
#include "curl/curl.h"
#include <iostream>
#include <memory>

class cURLWrapper
{
	static void Ref();
	CURL* _curl;
	std::shared_ptr<std::iostream> _readSink;
	std::shared_ptr<std::iostream> _writeSink;
	std::string _errorMessage;

protected:
	static size_t cURLWrite(void* ptr, size_t size, size_t nmemb, void* user_data);

public:
	cURLWrapper();
	virtual ~cURLWrapper();

	operator CURL* () const;
	CURLcode Perform();

	std::string ErrorMessage() const {
		return _errorMessage;
	}
	std::shared_ptr<std::iostream> ResponseStream();
};


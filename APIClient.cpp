/*
 This sample C++ client used gRPC asynchronous methods
 for processing the stream.
*/
#include <thread>
#include "APIClient.h"
#include <grpc++/create_channel.h>

#if defined(_WIN32)
#include <wincrypt.h>
// load root certificate authority from Windows System store
// and build a PEM string for gRPC
std::string get_root_ca()
{
	std::string cert;
	HCERTSTORE hStore = CertOpenSystemStore(0, "ROOT");
	if (hStore == NULL) {
		return "";
	}

	PCCERT_CONTEXT pContext = NULL;
	char cert_string[8192] = { 0 };
	while ((pContext = CertEnumCertificatesInStore(hStore, pContext)) != NULL) {
		DWORD cert_string_size = sizeof(cert_string);
		if (CryptBinaryToString(pContext->pbCertEncoded, pContext->cbCertEncoded,
			CRYPT_STRING_BASE64HEADER, cert_string, &cert_string_size))
		{
			cert += cert_string;
		}
	}

	CertFreeCertificateContext(pContext);
	CertCloseStore(hStore, 0);
	return std::move(cert);
}
#endif

using namespace AppTek;

APIClient::APIClient(const APIToken & api_token) :
	_api_token(api_token)
{
	if(_api_token.IsValid())
	{
		grpc::SslCredentialsOptions options;
#if defined(_WIN32)
		options.pem_root_certs = get_root_ca();
#endif
		_grpc_channel =
			grpc::CreateChannel(_api_token.Gateway(), grpc::SslCredentials(options));
	}
}

APIClient::~APIClient()
{

}

// Checks channel status and forces connection attempt.
bool APIClient::IsConnected()
{
	grpc_connectivity_state channel_state = _grpc_channel->GetState(true);
	if (channel_state != GRPC_CHANNEL_READY)
	{
		_grpc_channel->WaitForConnected(std::chrono::system_clock::now() +
			std::chrono::milliseconds(5000));
		channel_state = _grpc_channel->GetState(false);
	}
	return channel_state == GRPC_CHANNEL_READY;
}

// Get available services from gateway
bool APIClient::GetAvailable(gateway::RecognizeAvailableResponse & response)
{
	std::unique_ptr<grpc::ClientContext> context;
	std::unique_ptr<gateway::Gateway::Stub> gateway;
	gateway::AvailableRequest request;
	
	if (!IsConnected())
		return false;
	
	context =
		std::make_unique<grpc::ClientContext>();
	gateway =
		gateway::Gateway::NewStub(_grpc_channel);

	request.set_license_token(_api_token.Token());

	return
		gateway->RecognizeGetAvailable(context.get(), request, &response).ok();
}

// Create a new recognition stream which
std::unique_ptr<RecognizeStream> APIClient::CreateRecognizeStream(
	std::string const & language, int sample_rate,
	std::string const & translate_to_language)
{
	if (!IsConnected())
		return nullptr;

	std::unique_ptr<RecognizeStream> stream = 
		std::make_unique<RecognizeStream>(_grpc_channel);

	if (stream)
	{
		// Send the configuration.
		// It must be the first message transmitted
		gateway::RecognizeStreamRequest request;
		request.clear_audio();
		gateway::RecognizeStreamConfig * config = request.mutable_configuration();
		config->set_license_token(_api_token.Token());
		gateway::RecognizeAudioConfig * audio_config = config->mutable_audio_configuration();
		audio_config->set_encoding(gateway::RecognizeAudioEncoding::PCM_16bit_SI_MONO);
		audio_config->set_lang_code(language);
		audio_config->set_sample_rate_hz(sample_rate);

		if (translate_to_language.length())
		{
			gateway::RecognizeTranslateConfig* translate_config = config->mutable_translate_configuration();
			translate_config->set_target_lang_code(translate_to_language);
		}
		bool err = false;
		if (stream->CanWrite(err) && !err)
		{
			stream->_stream->Write(request, nullptr);
			return stream;
		}
	}
	return nullptr;
}

// Our recognizer stream class which maintains the state for the underlying gRPC stream
RecognizeStream::RecognizeStream(
	std::shared_ptr<grpc::Channel> & grpc_channel)
{
	_context = std::make_unique<grpc::ClientContext>();
	_gateway = gateway::Gateway::NewStub(grpc_channel);	
	_write_pending = true;
	// Use asynchronous gRPC stream so we don't block on reads
	_stream = _gateway->AsyncRecognizeStream(_context.get(), &_grpc_cq, nullptr);
	// setup read
	_read_pending = true;
	_stream->Read(&_next_response, &_next_response);
}

RecognizeStream::~RecognizeStream()
{

}

// Check if there is a write or read completion event
//
// Returns false if there is no write in progress indicating
// we can send an audio packet
bool RecognizeStream::CanWrite(bool & error)
{
	void* tag;
	bool ok = true;
	bool result = false;

	if ((_write_pending || _read_pending) && _grpc_cq.Next(&tag, &ok))
	{
		if (tag == (void*)&_next_response)
		{
			if (ok)
			{
				_response = _next_response;
				_next_response.Clear();

				_read_pending = true;
				// setup next read
				_stream->Read(&_next_response, &_next_response);
			}
		}
		else if (tag == nullptr)
		{
			result = true;
			_write_pending = false;
		}
	}
	error = !ok;

	return result || !_write_pending;
}

// Send audio packet
bool RecognizeStream::Stream(std::vector<char>audio_data, size_t length)
{
	// we can't send another packet while
	// a previous write is still pending
	bool result = !_write_pending;
	
	if (!_write_pending && length)
	{
		gateway::RecognizeStreamRequest request;
		std::string * data = request.mutable_audio();
		data->append(audio_data.data(), length);
		_write_pending = true;
		_stream->Write(request, nullptr);
	}

	return result;
}

// Check if response has been received
bool RecognizeStream::Response(gateway::RecognizeStreamResponse & response, bool & error)
{
	if (_response.oneof_response_case() ==
		gateway::RecognizeStreamResponse::OneofResponseCase::ONEOF_RESPONSE_NOT_SET &&
		_read_pending &&
		_writes_done)
	{
		// check completion queue
		CanWrite(error);
	}
	if (_response.oneof_response_case() != 
		gateway::RecognizeStreamResponse::OneofResponseCase::ONEOF_RESPONSE_NOT_SET)
	{
		response = std::move(_response);
		_response.Clear();
		return true;
	}
	return false;
}

// Tell gRPC we are done sending audio data
bool RecognizeStream::WritesDone(bool & error)
{
	if (CanWrite(error) && !error)
	{
		_stream->WritesDone(nullptr);
		_writes_done = true;
		return true;
	}

	return false;
}

// Requests AppTek Streaming ASR Gateway token
#include "APILicense.h"
#include "cURLWrapper.h"

using namespace AppTek;

APILicense::APILicense()
{

}

APILicense::APILicense(const std::string & api_key)
{
	Get(api_key);
}

APILicense::~APILicense()
{

}

bool APILicense::Get(const std::string & api_key)
{
	cURLWrapper curl;
	std::string url = "https://license.apptek.com/license/v2/token/" + api_key;
	curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_CONNECT_ONLY, 0L);

	CURLcode result = curl.Perform();
	if (result == CURLE_OK)
	{
		std::shared_ptr<std::iostream> response_stream = curl.ResponseStream();

		response_stream->seekg(0, response_stream->end);
		u_int response_length = (u_int)response_stream->tellg();
		response_stream->seekg(0, response_stream->beg);

		if (response_length > 0)
		{
			std::string response;
			response.resize(response_length);
			response_stream->read(const_cast<char*>(response.data()), response_length);
			return _api_token.Set(response);			
		}
	}
	else
		_err_message = curl.ErrorMessage();

	return false;
}

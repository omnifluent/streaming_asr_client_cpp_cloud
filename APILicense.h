// Requests AppTek Streaming ASR Gateway token
#pragma once
#include <string>
#include "APIToken.h"

namespace AppTek {

	class APILicense
	{
		APIToken _api_token;
		std::string _err_message;

	public:
		APILicense();
		APILicense(const std::string& api_key);
		~APILicense();

		bool Get(const std::string& api_key);

		const APIToken& Token()
		{
			return _api_token;
		}
		std::string ErrorMessage() const
		{
			return _err_message;
		}
	};

} // namespace AppTek


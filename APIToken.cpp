// Parse and decode client-side portion of AppTek Streaming ASR API token
#include <memory>
#include "APIToken.h"
#include "contrib/jsoncpp/json/json.h"
#include "contrib/cpp-base64/base64.h"

using namespace AppTek;

APIToken::APIToken()
{

}

APIToken::APIToken(std::string const& str_token)
{
	if (str_token.length())
		Set(str_token);
}

bool APIToken::Set(std::string const& str_token)
{
	_api_token_json.clear();

	size_t delim = str_token.find_first_of('.');
	if (delim > 1)
	{
		std::string client_token =
			base64_decode(str_token.substr(delim + 1, str_token.find_first_of('.', delim + 1) - delim - 1));
		if (client_token.length())
		{
			Json::CharReaderBuilder builder;
			std::unique_ptr<Json::CharReader> reader(builder.newCharReader());

			if (reader->parse(client_token.data(), client_token.data() + client_token.length(), &_api_token_json, &_error))
			{
				_str_token = str_token;
				return true;
			}
		}
		else
			_error = "Token decode failed";
	}
	else
		_error = "Invalid API token";

	return false;
}

bool APIToken::IsValid() const
{
	return _str_token.length() && !_api_token_json.isNull();
}

std::string APIToken::Token() const
{
	return _str_token;
}

std::string APIToken::Gateway() const
{
	return std::move(_api_token_json["host"].asString() + ":" + _api_token_json["port"].asString());
}